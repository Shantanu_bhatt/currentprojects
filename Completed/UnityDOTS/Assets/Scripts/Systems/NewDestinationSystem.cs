using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class NewDestinationSystem : SystemBase
{
    private RandomSystem randomSystem;
    protected override void OnCreate()
    {
        randomSystem=World.GetExistingSystem<RandomSystem>();
    }
    protected override void OnUpdate()
    {
        var randomArray=randomSystem.RandomArray;
        
        
        
        Entities.WithNativeDisableParallelForRestriction(randomArray).ForEach((int nativeThreadIndex,ref Destination destination, in Translation translation) => {
            float dist=math.abs(math.length(destination.Value-translation.Value));
            if(dist<0.1f)
            {
                var random=randomArray[nativeThreadIndex];
                destination.Value.x=random.NextFloat(0,150);
                destination.Value.z=random.NextFloat(0,150);
                randomArray[nativeThreadIndex]=random;
            }
        }).ScheduleParallel();
    }
}
