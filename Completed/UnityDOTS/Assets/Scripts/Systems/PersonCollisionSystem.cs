using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics.Systems;
using Unity.Physics;
using Unity.Rendering;
public class PersonCollisionSystem : SystemBase
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    protected override void OnCreate()
    {
        buildPhysicsWorld=World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld=World.GetOrCreateSystem<StepPhysicsWorld>();
    }

    struct CollisionJob : ITriggerEventsJob
    {
        public ComponentDataFromEntity<URPMaterialPropertyBaseColor> ColorGroup;
        [ReadOnly]public ComponentDataFromEntity<PersonTag> PersonGroup;
        public float Seed;
        public void Execute(TriggerEvent triggerEvent)
        {
           bool isEntityAPerson=PersonGroup.HasComponent(triggerEvent.EntityA);
           bool isEntityBPerson=PersonGroup.HasComponent(triggerEvent.EntityB);
           
            if(!isEntityAPerson || !isEntityBPerson){return;}
            var random=new Unity.Mathematics.Random((uint)((triggerEvent.BodyIndexA*triggerEvent.BodyIndexB)*(1+Seed) +1));
            random=ChangelMaterialColor(random,triggerEvent.EntityA);
            random=ChangelMaterialColor(random,triggerEvent.EntityB);
        }

        Random ChangelMaterialColor(Random random,Entity entity)
    {
        if(ColorGroup.HasComponent(entity))
        {
            var colorComponent=ColorGroup[entity];
            colorComponent.Value.x=random.NextFloat(0,1);
            colorComponent.Value.y=random.NextFloat(0,1);
            colorComponent.Value.z=random.NextFloat(0,1);
            ColorGroup[entity]=colorComponent;
            
        }
        return random;
    }
    }

    
    protected override void OnUpdate()
    {
        Dependency=new CollisionJob{
            PersonGroup=GetComponentDataFromEntity<PersonTag>(true),
            ColorGroup=GetComponentDataFromEntity<URPMaterialPropertyBaseColor>(),
            Seed=System.DateTimeOffset.Now.Millisecond
        }.Schedule(stepPhysicsWorld.Simulation,ref buildPhysicsWorld.PhysicsWorld,Dependency);
    }
}

