using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class LifeTimeSystem : SystemBase
{
    public EndSimulationEntityCommandBufferSystem endSimECBSystem;
    protected override void OnCreate()
    {
        endSimECBSystem=World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }
    protected override void OnUpdate()
    {
        
        float deltaTime = Time.DeltaTime;
        var ecb=endSimECBSystem.CreateCommandBuffer().AsParallelWriter();
        
        
        
        
        Entities.ForEach((Entity entity,int entityInQueryIndex,ref LifeTime lifeTime, in Rotation rotation) => {
            lifeTime.Value-=deltaTime;
            
            if(lifeTime.Value<=0)
            {
                ecb.DestroyEntity(entityInQueryIndex,entity);
            }

           
        }).ScheduleParallel();
        endSimECBSystem.AddJobHandleForProducer(Dependency);
    }
}
