

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;


public class SetterSpawner : MonoBehaviour
{
    [SerializeField] private GameObject _personPrefab;
    [SerializeField] private int _gridSize;
    [SerializeField] private int spread;
    [SerializeField] float2 speedRange=new float2(4,7);

    BlobAssetStore blob;
    void Start()
    {
        blob=new BlobAssetStore();
        var settings=GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld,blob);
        var entity=GameObjectConversionUtility.ConvertGameObjectHierarchy(_personPrefab,settings);
        var entityManager=World.DefaultGameObjectInjectionWorld.EntityManager;
        for(int i=0;i<_gridSize;i++)
        {
            for(int j=0;j<_gridSize;j++)
            {
                var instance=entityManager.Instantiate(entity);
                float3 position= new float3(i*spread,0,j*spread);
                float moveSpeed= UnityEngine.Random.Range(speedRange.x,speedRange.y);
                entityManager.SetComponentData(instance,new Translation{Value= position});
                entityManager.SetComponentData(instance,new MovementSpeed{Value=moveSpeed});
                entityManager.SetComponentData(instance, new Destination{Value=position});
                entityManager.SetComponentData(instance,new LifeTime{Value=UnityEngine.Random.Range(1f,20f)});
            }
        }
        
    }

    private void OnDestroy() {
        blob.Dispose();
    }

    
}
