﻿using UnityEngine;
using System.Collections;

public class PassData : MonoBehaviour
{

    public ComputeShader shader;
    public int texResolution = 1024;
    public int count=10;

    struct Circle{
        public Vector2 origin;
        public Vector2 velocity;
        public float radius;
    }
    Renderer rend;
    RenderTexture outputTexture;

    int circlesHandle;
    int clearHandle;

    public Color clearColor = new Color();
    public Color circleColor = new Color();

    Circle[] circleData;
    ComputeBuffer buffer;

    // Use this for initialization
    void Start()
    {
        outputTexture = new RenderTexture(texResolution, texResolution, 0);
        outputTexture.enableRandomWrite = true;
        outputTexture.Create();

        rend = GetComponent<Renderer>();
        rend.enabled = true;

        InitShader();
    }

    private void InitShader()
    {
        circlesHandle = shader.FindKernel("Circles");
        Debug.Log(circlesHandle);
        clearHandle=shader.FindKernel("Clear");
        uint threadGroupSizeX;
        shader.GetKernelThreadGroupSizes(circlesHandle,out threadGroupSizeX,out _,out _);
        int total= (int)threadGroupSizeX*count;
        circleData=new Circle[total];

        float speed=100f;
        float halfSpeed=50f;
        float minRad=10f;
        float maxRad=30f;
        float radiusRange=maxRad-minRad;
        for(int i=0;i<total;i++)
        {
            Circle circle=circleData[i];
            circle.origin.x=Random.value*texResolution;
            
            circle.origin.y=Random.value*texResolution;
            
            circle.velocity.x=Random.value*speed-halfSpeed;
            circle.velocity.y=Random.value*speed-halfSpeed;
           
            circle.radius=Random.Range(minRad,maxRad);
             
            circleData[i]=circle;

        }
        shader.SetVector("clearColor",clearColor);
        shader.SetVector("circleColor",circleColor);
        shader.SetInt( "texResolution", texResolution);
        shader.SetTexture( circlesHandle, "Result", outputTexture);
        shader.SetTexture( clearHandle, "Result", outputTexture);
        rend.material.SetTexture("_MainTex", outputTexture);
        int stride=(2+2+1)*sizeof(float);
        buffer= new ComputeBuffer(circleData.Length,stride);
        buffer.SetData(circleData);
        Debug.Log(stride);
        shader.SetBuffer(circlesHandle,"circlesBuffer",buffer);
    }
 
    private void DispatchKernels(int count)
    {
        shader.Dispatch(clearHandle,texResolution/8,texResolution/8,1);
        shader.SetFloat("time",Time.time);
        shader.Dispatch(circlesHandle, count, 1, 1);
        
    }

    void Update()
    {
        DispatchKernels(count);
    }
}

