﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Launcher::Start()
extern void Launcher_Start_mBD1E6306A64712A170916699B4BE6AA6BD32E37E (void);
// 0x00000002 System.Void Launcher::Awake()
extern void Launcher_Awake_m51131F9D53E49BF22DBF64A5B3CC40A475058D1A (void);
// 0x00000003 System.Void Launcher::OnConnectedToMaster()
extern void Launcher_OnConnectedToMaster_mBE4CB3AA2DF5AF2BE55F7A31C7520372408F75A5 (void);
// 0x00000004 System.Void Launcher::OnJoinedLobby()
extern void Launcher_OnJoinedLobby_m1B40410F13151F14C74CDF85280285C90253C6E7 (void);
// 0x00000005 System.Void Launcher::CreateRoom()
extern void Launcher_CreateRoom_m1F5EFF5B4E97EE8AAF3D683BF6CCCF9309A5F389 (void);
// 0x00000006 System.Void Launcher::OnJoinedRoom()
extern void Launcher_OnJoinedRoom_m070B91E213700375EF7458F10138C155C3CEA22F (void);
// 0x00000007 System.Void Launcher::OnMasterClientSwitched(Photon.Realtime.Player)
extern void Launcher_OnMasterClientSwitched_mA2FB07F9E75ADDEB4CF269A85BB3C4C2F0D09F75 (void);
// 0x00000008 System.Void Launcher::JoinRoom(Photon.Realtime.RoomInfo)
extern void Launcher_JoinRoom_m5F8FE9667B304077DBBDF9F661FED2C806D5080D (void);
// 0x00000009 System.Void Launcher::OnCreateRoomFailed(System.Int16,System.String)
extern void Launcher_OnCreateRoomFailed_m69653B4F9C2BC16C4E8EFD994B023F58A34B72A7 (void);
// 0x0000000A System.Void Launcher::LeaveRoom()
extern void Launcher_LeaveRoom_m17A2F2FB67A3080243D4450F1796708EC1D7FEBE (void);
// 0x0000000B System.Void Launcher::OnLeftRoom()
extern void Launcher_OnLeftRoom_mC9D36826DBFF6B0059391F99B888925C59DB4B77 (void);
// 0x0000000C System.Void Launcher::OnRoomListUpdate(System.Collections.Generic.List`1<Photon.Realtime.RoomInfo>)
extern void Launcher_OnRoomListUpdate_mB7315F356D99B384839DD7A9459BAF51AF941E8F (void);
// 0x0000000D System.Void Launcher::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void Launcher_OnPlayerEnteredRoom_mA47224DCA42162C343F78E7EFB5A79B66C22CCC2 (void);
// 0x0000000E System.Void Launcher::StartGame()
extern void Launcher_StartGame_m1929F43379C54CA09AF8A00DC0FE7E1B040C83E1 (void);
// 0x0000000F System.Void Launcher::.ctor()
extern void Launcher__ctor_mB177F9E0B472FC0208A5EF8E702BD3712C0AED96 (void);
// 0x00000010 System.Void Menu::Open()
extern void Menu_Open_mAE2961F7F13C7085320D41A6AC36DE4055D00D32 (void);
// 0x00000011 System.Void Menu::Close()
extern void Menu_Close_m36F6ECC1DE557D44B4588076226A1D71FF8451E5 (void);
// 0x00000012 System.Void Menu::.ctor()
extern void Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134 (void);
// 0x00000013 System.Void MenuManager::Awake()
extern void MenuManager_Awake_mEFBAF3F8CBDEF5A033B3BAD9CA897801135B6463 (void);
// 0x00000014 System.Void MenuManager::OpenMenu(System.String)
extern void MenuManager_OpenMenu_m5D1B741F99822E35C5375C6DB01389A6ECB08D09 (void);
// 0x00000015 System.Void MenuManager::OpenMenu(Menu)
extern void MenuManager_OpenMenu_mC076A53AAF0FB8F2CF531BD124FE44738C4D359A (void);
// 0x00000016 System.Void MenuManager::CloseMenu(Menu)
extern void MenuManager_CloseMenu_m5AAA343AF3BE130D987F7DD37E42AC8855C5D196 (void);
// 0x00000017 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x00000018 System.Void PlayerListItem::Setup(Photon.Realtime.Player)
extern void PlayerListItem_Setup_m37B55A036CBE96EEC37C300BACBA39BFC3DCC978 (void);
// 0x00000019 System.Void PlayerListItem::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void PlayerListItem_OnPlayerLeftRoom_m7BE0EAB716C6EF673C63F63B23FDCBA2310EE706 (void);
// 0x0000001A System.Void PlayerListItem::.ctor()
extern void PlayerListItem__ctor_m7C8496D1D1F19D6A371A782E74B436B15F4A87A3 (void);
// 0x0000001B System.Void PlayerName::Start()
extern void PlayerName_Start_mEAB6D4DA8FC9F22B98BC2BD26FB8EBD83FCEDD3F (void);
// 0x0000001C System.Void PlayerName::onUserNameInputValueChanged()
extern void PlayerName_onUserNameInputValueChanged_m250E5B03B675214EA929F3AD4FF72DAFEB14C0E9 (void);
// 0x0000001D System.Void PlayerName::.ctor()
extern void PlayerName__ctor_m823B662B9836F22D25C3E8543975C64451CB2E1C (void);
// 0x0000001E System.Void RoomListItem::Setup(Photon.Realtime.RoomInfo)
extern void RoomListItem_Setup_m0D2DFC4693D420BD2A6C71703C483A1432AAD7FA (void);
// 0x0000001F System.Void RoomListItem::OnClick()
extern void RoomListItem_OnClick_mFDBF25695DFA8123556FB6852F4C44B404A4F9B0 (void);
// 0x00000020 System.Void RoomListItem::.ctor()
extern void RoomListItem__ctor_mD67FF455921E65330604613BE05C59A33DC2E5DC (void);
// 0x00000021 System.Void RoomManager::Awake()
extern void RoomManager_Awake_m1B0A42856399F2B323B12A1EC2313430753F2676 (void);
// 0x00000022 System.Void RoomManager::OnEnable()
extern void RoomManager_OnEnable_m9296F4D0D35A91B5DE105E9ACCF3A3710798AA95 (void);
// 0x00000023 System.Void RoomManager::OnDisable()
extern void RoomManager_OnDisable_m1AD6576F43FBB141C67E93D81EC9F3BB22D241ED (void);
// 0x00000024 System.Void RoomManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void RoomManager_OnSceneLoaded_m913468D79B41E00B21BB4EAC7E8414077F110A99 (void);
// 0x00000025 System.Void RoomManager::.ctor()
extern void RoomManager__ctor_mB80958CB1C8945EFECA6721A48CCD7FD4FD642C0 (void);
// 0x00000026 System.Void SpawnManager::Awake()
extern void SpawnManager_Awake_mAA4782E6BB38FD064066134F836BFC0D1E22C5ED (void);
// 0x00000027 UnityEngine.Transform SpawnManager::GetSpawnPoint()
extern void SpawnManager_GetSpawnPoint_m05A33D472D07BB860BBFA3C47F405A583C4C809B (void);
// 0x00000028 System.Void SpawnManager::.ctor()
extern void SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59 (void);
// 0x00000029 System.Void SpawnPoint::Awake()
extern void SpawnPoint_Awake_mF1B511096D33AF531699E45CF8DCF401EFC03BB9 (void);
// 0x0000002A System.Void SpawnPoint::.ctor()
extern void SpawnPoint__ctor_m5A0EE1AE5C5886E3EAB22116C74D10952EA35D68 (void);
// 0x0000002B System.Void RotateOrb::Update()
extern void RotateOrb_Update_m586CE3DDA5497ABAFD489BBF8F9AEFC97C0DCF51 (void);
// 0x0000002C System.Void RotateOrb::.ctor()
extern void RotateOrb__ctor_m2BF45F05C309A0E575A924386251900B7BA155DE (void);
// 0x0000002D System.Void Shake::Start()
extern void Shake_Start_m1600C826C2AEC8845D2DA2C79529EB5ADF34361F (void);
// 0x0000002E System.Void Shake::Update()
extern void Shake_Update_mD4180E20FAC0B5C31E2E8F4625FA0F102EB944E6 (void);
// 0x0000002F System.Void Shake::.ctor()
extern void Shake__ctor_mF717CF705450396F4E4D67B34334C90632133B31 (void);
// 0x00000030 System.Void BillBoard::Update()
extern void BillBoard_Update_mD3CE536C88D1A216DFA678AF603375ADE6A7F103 (void);
// 0x00000031 System.Void BillBoard::.ctor()
extern void BillBoard__ctor_mCA6D5C7FA62AD3E598CF7B07D30E81F954C9DAF8 (void);
// 0x00000032 System.Void CameraFollow::Update()
extern void CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08 (void);
// 0x00000033 System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE (void);
// 0x00000034 System.Void DeleteFlash::Start()
extern void DeleteFlash_Start_m1DEADFEE487364DBA863B51153F19C5C17025143 (void);
// 0x00000035 System.Collections.IEnumerator DeleteFlash::Delete()
extern void DeleteFlash_Delete_m8C1A4F0ABF27BF6C2BD480B0F6995985CC795890 (void);
// 0x00000036 System.Void DeleteFlash::.ctor()
extern void DeleteFlash__ctor_m8F958FE65ACD1D1E644FCA416E24078212516E8F (void);
// 0x00000037 System.Void DeleteFlash/<Delete>d__1::.ctor(System.Int32)
extern void U3CDeleteU3Ed__1__ctor_m216875B50A3B3025AE303EFE9ED9B99F8EA2D38F (void);
// 0x00000038 System.Void DeleteFlash/<Delete>d__1::System.IDisposable.Dispose()
extern void U3CDeleteU3Ed__1_System_IDisposable_Dispose_mD6EBB2645B251514EB99201C802CF5BBB0A51379 (void);
// 0x00000039 System.Boolean DeleteFlash/<Delete>d__1::MoveNext()
extern void U3CDeleteU3Ed__1_MoveNext_mC0A5143E3663F1FCE949CD9581C7F05C3BDBB72D (void);
// 0x0000003A System.Object DeleteFlash/<Delete>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeleteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52760247A64E356F41E4467EABEE4D3D269FDD43 (void);
// 0x0000003B System.Void DeleteFlash/<Delete>d__1::System.Collections.IEnumerator.Reset()
extern void U3CDeleteU3Ed__1_System_Collections_IEnumerator_Reset_m42D86D8CA78EB232F7C0C5B1E3BA0FB8D7152A1B (void);
// 0x0000003C System.Object DeleteFlash/<Delete>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CDeleteU3Ed__1_System_Collections_IEnumerator_get_Current_mEFA12E89EDC56CD75DFAD58CB66583DEA7B17402 (void);
// 0x0000003D System.Void GunControl::Start()
extern void GunControl_Start_mE1294B9657EA97AF63F6E6428D44E928BE707CF9 (void);
// 0x0000003E System.Void GunControl::Fire()
extern void GunControl_Fire_m3D7F067AC3E8CF23ACD03B70E08F858353CF7570 (void);
// 0x0000003F System.Collections.IEnumerator GunControl::FireRate()
extern void GunControl_FireRate_mF82F43A4A5DD8F17764A4EEA1ED41934247D53C8 (void);
// 0x00000040 System.Collections.IEnumerator GunControl::SpreadReset()
extern void GunControl_SpreadReset_mBCF8794FEB70233376243A6DD993D62B9E0E8A95 (void);
// 0x00000041 System.Collections.IEnumerator GunControl::Reload()
extern void GunControl_Reload_m3FA7F34FDF457C583383B420297FF8B155C45536 (void);
// 0x00000042 System.Void GunControl::RPC_Shoot(UnityEngine.Vector3)
extern void GunControl_RPC_Shoot_m1ECE74CFED1B6F35B33519958BDD58259B6E10C7 (void);
// 0x00000043 System.Void GunControl::.ctor()
extern void GunControl__ctor_m6F7C4242AC4BC4F17EB0D81974E4ACBE2FCB4001 (void);
// 0x00000044 System.Void GunControl/<FireRate>d__20::.ctor(System.Int32)
extern void U3CFireRateU3Ed__20__ctor_mF7B23521EC7A14A2C73B6C4D31233ECB83726B55 (void);
// 0x00000045 System.Void GunControl/<FireRate>d__20::System.IDisposable.Dispose()
extern void U3CFireRateU3Ed__20_System_IDisposable_Dispose_mAABF726483CB5951DDB3DFC7AAF7256655E80008 (void);
// 0x00000046 System.Boolean GunControl/<FireRate>d__20::MoveNext()
extern void U3CFireRateU3Ed__20_MoveNext_m44CB144ABEE69651A6F58E1CAA5AEE05A279C748 (void);
// 0x00000047 System.Object GunControl/<FireRate>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireRateU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1323AD636CA63A9B03737772BF8485FEB063EC14 (void);
// 0x00000048 System.Void GunControl/<FireRate>d__20::System.Collections.IEnumerator.Reset()
extern void U3CFireRateU3Ed__20_System_Collections_IEnumerator_Reset_m8CB91B9CB3E5A883A9E900C450E4E1F69F2049CF (void);
// 0x00000049 System.Object GunControl/<FireRate>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CFireRateU3Ed__20_System_Collections_IEnumerator_get_Current_m986D678AA608DE4C57F8194957D48CE1F57B6F97 (void);
// 0x0000004A System.Void GunControl/<SpreadReset>d__21::.ctor(System.Int32)
extern void U3CSpreadResetU3Ed__21__ctor_m9EA8131BA00AADDFD24366141DDC1C43DAC1D9D0 (void);
// 0x0000004B System.Void GunControl/<SpreadReset>d__21::System.IDisposable.Dispose()
extern void U3CSpreadResetU3Ed__21_System_IDisposable_Dispose_m4FC73AB61941AEC1032DBC20421080D7D3E40B67 (void);
// 0x0000004C System.Boolean GunControl/<SpreadReset>d__21::MoveNext()
extern void U3CSpreadResetU3Ed__21_MoveNext_m5FD7B1A203B19C0E95FFEE969C83597AB307B644 (void);
// 0x0000004D System.Object GunControl/<SpreadReset>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpreadResetU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m007B89432E455B7F770CFD41E7E9106CE4250766 (void);
// 0x0000004E System.Void GunControl/<SpreadReset>d__21::System.Collections.IEnumerator.Reset()
extern void U3CSpreadResetU3Ed__21_System_Collections_IEnumerator_Reset_m16F4AA565706420FDCC40C6F1A04C19A2198190B (void);
// 0x0000004F System.Object GunControl/<SpreadReset>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CSpreadResetU3Ed__21_System_Collections_IEnumerator_get_Current_mA6EA8E0EFE6130276101F09788543A37BF878654 (void);
// 0x00000050 System.Void GunControl/<Reload>d__22::.ctor(System.Int32)
extern void U3CReloadU3Ed__22__ctor_m2201D70CCAE341BDDBEA33527A97C1800991EC7A (void);
// 0x00000051 System.Void GunControl/<Reload>d__22::System.IDisposable.Dispose()
extern void U3CReloadU3Ed__22_System_IDisposable_Dispose_m3D2BB7FB141F871A9E08A6029D6CA0B9F14B9A47 (void);
// 0x00000052 System.Boolean GunControl/<Reload>d__22::MoveNext()
extern void U3CReloadU3Ed__22_MoveNext_m5908F49995FB131887058CEF4ECA9A7A3839E67F (void);
// 0x00000053 System.Object GunControl/<Reload>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReloadU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC49E3DBB145B502ACBFAC33B91DD737D815024A (void);
// 0x00000054 System.Void GunControl/<Reload>d__22::System.Collections.IEnumerator.Reset()
extern void U3CReloadU3Ed__22_System_Collections_IEnumerator_Reset_m2D383BC1403BD8A5C485E0CB6F88636A9DF28C06 (void);
// 0x00000055 System.Object GunControl/<Reload>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CReloadU3Ed__22_System_Collections_IEnumerator_get_Current_m0E35258881CF80F7B7A70077DA7E848A471CFE50 (void);
// 0x00000056 System.Void IDamageable::TakeDamage(System.Int32,System.Int32)
// 0x00000057 System.Void SetLayer::Awake()
extern void SetLayer_Awake_m1FE4DB1B05F4E2371460F04DC5155077FFBCFFC1 (void);
// 0x00000058 System.Void SetLayer::.ctor()
extern void SetLayer__ctor_m4DF136ADF2483F9A2BF84B67D0E07C0648E7B9F0 (void);
// 0x00000059 System.Void OrbSpawn::Start()
extern void OrbSpawn_Start_m37455446469CD476F0FFC80A23B66BB38EDF289A (void);
// 0x0000005A System.Void OrbSpawn::Update()
extern void OrbSpawn_Update_mB09656515477DD69631E91EE4C0890C9333475AF (void);
// 0x0000005B System.Void OrbSpawn::FireOrb()
extern void OrbSpawn_FireOrb_mEE78F143BFE55D4FD1C3B49DE21FFED1A21BF8D6 (void);
// 0x0000005C System.Void OrbSpawn::FixedUpdate()
extern void OrbSpawn_FixedUpdate_m6083F8D53C0D1ED1401CAA20E486C3AE44CB9119 (void);
// 0x0000005D System.Void OrbSpawn::.ctor()
extern void OrbSpawn__ctor_m382735D6036D759826F84534769768DDCB679474 (void);
// 0x0000005E System.Void PlayerLook::Start()
extern void PlayerLook_Start_m6F42762E7F9161934F3A942F2247F73A2FFE61A5 (void);
// 0x0000005F System.Void PlayerLook::Update()
extern void PlayerLook_Update_mA425AA6701F39303BF7744B1A2E2E2B41BAC6793 (void);
// 0x00000060 System.Void PlayerLook::Inputs()
extern void PlayerLook_Inputs_mABDD75FEDB60BD077BFCC696EF6079933BB3B723 (void);
// 0x00000061 System.Void PlayerLook::.ctor()
extern void PlayerLook__ctor_mF86A13B977B997D8F4BE67197972675B6AC09456 (void);
// 0x00000062 System.Void PlayerManager::Awake()
extern void PlayerManager_Awake_m4E0FE19C5F34114E28F28F04CCF33C2E34C743E7 (void);
// 0x00000063 System.Void PlayerManager::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void PlayerManager_OnPlayerEnteredRoom_mF47D9E64F878E27349BFFB066949DFD90B4945FF (void);
// 0x00000064 System.Void PlayerManager::Start()
extern void PlayerManager_Start_mA587FD881326FC1FBCEE2699E54A8A0ACCF83455 (void);
// 0x00000065 System.Void PlayerManager::CreateController()
extern void PlayerManager_CreateController_m69B81D159BF3BF6508A16733FC7725E731C47C29 (void);
// 0x00000066 System.Void PlayerManager::Die()
extern void PlayerManager_Die_m1FDAE7B1320B80ED49DFF09BFE7ABEAD58EF96F2 (void);
// 0x00000067 System.Void PlayerManager::UpdateKills()
extern void PlayerManager_UpdateKills_mDCF2BF1B361446BCFB9F1B870B7B21CB4A001FBB (void);
// 0x00000068 System.Void PlayerManager::UpdateScoreBoard()
extern void PlayerManager_UpdateScoreBoard_mAA33450EB7DF52AB83DD993F1F54F63102E1F424 (void);
// 0x00000069 System.Void PlayerManager::RPC_UpdateScoreBoard(Photon.Realtime.Player,System.Int32,System.Int32)
extern void PlayerManager_RPC_UpdateScoreBoard_m5CF310A54C464E6042B1C29AA5AD21965C5F2B45 (void);
// 0x0000006A System.Void PlayerManager::.ctor()
extern void PlayerManager__ctor_m4C7CA12A8243D6CA73C1EA65B361E7B717070471 (void);
// 0x0000006B System.Boolean PlayerMovement::OnSlope()
extern void PlayerMovement_OnSlope_m0FEB636046795FABC82F61549935B2EE10C63568 (void);
// 0x0000006C System.Void PlayerMovement::Awake()
extern void PlayerMovement_Awake_m441E25AABE54B8C5068808DB8025B67B9A7EA87E (void);
// 0x0000006D System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6 (void);
// 0x0000006E System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x0000006F System.Void PlayerMovement::CanvasUpdate()
extern void PlayerMovement_CanvasUpdate_m7DA2BEFBFBB51D0923215C0BB7DFF53A889D248C (void);
// 0x00000070 System.Void PlayerMovement::ControlSpeed()
extern void PlayerMovement_ControlSpeed_m0B7CD52D0A6447607E08A1009138B2FB6D31C39A (void);
// 0x00000071 System.Void PlayerMovement::Jump()
extern void PlayerMovement_Jump_m18B01D49691550104FB03CBFE5060DA675CC8F7E (void);
// 0x00000072 System.Void PlayerMovement::Inputs()
extern void PlayerMovement_Inputs_mDBF6B23AEB0F9445A81DB4CF8DD2F56C682D07DA (void);
// 0x00000073 System.Void PlayerMovement::ControlDrag()
extern void PlayerMovement_ControlDrag_m24019BCE1DC6B18B40E7D5AE26C716283AAEA619 (void);
// 0x00000074 System.Void PlayerMovement::FixedUpdate()
extern void PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499 (void);
// 0x00000075 System.Void PlayerMovement::MovePlayer()
extern void PlayerMovement_MovePlayer_m48A4FBFA7E90EFA604296D4294D677675C0C7786 (void);
// 0x00000076 System.Void PlayerMovement::EquipGun(System.Int32)
extern void PlayerMovement_EquipGun_m034ACE4BD2A95CF136F5797356BBFB14CD7F1995 (void);
// 0x00000077 System.Void PlayerMovement::OnPlayerPropertiesUpdate(Photon.Realtime.Player,ExitGames.Client.Photon.Hashtable)
extern void PlayerMovement_OnPlayerPropertiesUpdate_mB3E510B1412252F562BA60EBF20F78EC4C805B2F (void);
// 0x00000078 System.Void PlayerMovement::TakeDamage(System.Int32,System.Int32)
extern void PlayerMovement_TakeDamage_m102C3EBE5A69234CD0FB69C7D54131CAA2A06BF8 (void);
// 0x00000079 System.Void PlayerMovement::RPC_TakeDamage(System.Int32,System.Int32)
extern void PlayerMovement_RPC_TakeDamage_mF4BBA6C6FD6C77F01FC6A38015A213D46DDFB393 (void);
// 0x0000007A System.Void PlayerMovement::RPC_IncreaseKills(System.Int32)
extern void PlayerMovement_RPC_IncreaseKills_m806326A9DBC3539E5D25EF8EABBE515B7A61C48F (void);
// 0x0000007B System.Void PlayerMovement::Die()
extern void PlayerMovement_Die_mEE19591E6BB295276E46178F81CC568F23819B0B (void);
// 0x0000007C System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x0000007D System.Void UsernameDisplay::Start()
extern void UsernameDisplay_Start_mB27B58282D6B9D9347220C104CB0B6892E6777DB (void);
// 0x0000007E System.Void UsernameDisplay::.ctor()
extern void UsernameDisplay__ctor_mC84E68114906E4D9E28E4D91D59320B44D929BC7 (void);
// 0x0000007F System.Boolean Wallrunning::canWallRun()
extern void Wallrunning_canWallRun_mC02EC2E2722C5B9984243CB291A28113EA97BD5E (void);
// 0x00000080 System.Void Wallrunning::CheckWall()
extern void Wallrunning_CheckWall_m63072765AE2DE567B910A2942AB635B66A098A5B (void);
// 0x00000081 System.Void Wallrunning::Start()
extern void Wallrunning_Start_mEF2BD7D5CFC701E75B202138CBE4E5ADD0735B95 (void);
// 0x00000082 System.Void Wallrunning::Update()
extern void Wallrunning_Update_mA733766805FA0F470575A07A07F9904FF36D541E (void);
// 0x00000083 System.Void Wallrunning::StartWallRun()
extern void Wallrunning_StartWallRun_m3EADD4A014ABA65B010FAA0CD1658BCF86612D5C (void);
// 0x00000084 System.Void Wallrunning::StopWallRun()
extern void Wallrunning_StopWallRun_mC2C3E2EFA26961C7379EFA9DDE4D044809A5F8ED (void);
// 0x00000085 System.Void Wallrunning::.ctor()
extern void Wallrunning__ctor_mB36D52AEE4F02EC669E2E878CE0B40B1987B2F2D (void);
// 0x00000086 System.Void CanvasManager::SetCanvas(System.Int32,System.Int32,System.Int32,System.Int32)
extern void CanvasManager_SetCanvas_m9B3B1BDF91453AD6BD36523DD58D81DCE474F30E (void);
// 0x00000087 System.Void CanvasManager::.ctor()
extern void CanvasManager__ctor_mD320401BACAF61E10355C3BEDAF096DCDA817AC9 (void);
// 0x00000088 System.Void ScoreBoardItem::Initialize(Photon.Realtime.Player)
extern void ScoreBoardItem_Initialize_mD0E6DF486EA62EA84BB266A22383D25297DC9F32 (void);
// 0x00000089 System.Void ScoreBoardItem::.ctor()
extern void ScoreBoardItem__ctor_mD7C526C8F583A00D65B1C1E522366EE98A5C637A (void);
// 0x0000008A System.Void ScoreBoardManager::Start()
extern void ScoreBoardManager_Start_mEDC674AB7B64371CF0F9E8A09A05FDC864B8F24A (void);
// 0x0000008B System.Void ScoreBoardManager::AddScoreBoardItem(Photon.Realtime.Player)
extern void ScoreBoardManager_AddScoreBoardItem_m4FEF102126D5404B9D7503B6050E78A170B760F2 (void);
// 0x0000008C System.Void ScoreBoardManager::OnPlayerEnteredRoom(Photon.Realtime.Player)
extern void ScoreBoardManager_OnPlayerEnteredRoom_mB2D3B837CF2F5FA602EB9DBDA08228DF97A1B87B (void);
// 0x0000008D System.Void ScoreBoardManager::OnPlayerLeftRoom(Photon.Realtime.Player)
extern void ScoreBoardManager_OnPlayerLeftRoom_mC705709E1DF4F4BBE568282D4DA94B94863A7E5C (void);
// 0x0000008E System.Void ScoreBoardManager::RemoveScoreBoardItem(Photon.Realtime.Player)
extern void ScoreBoardManager_RemoveScoreBoardItem_m16B65D04B76E1623C067364F5EE7CA1CCC726ADC (void);
// 0x0000008F System.Void ScoreBoardManager::UpdateKD(Photon.Realtime.Player,System.Int32,System.Int32)
extern void ScoreBoardManager_UpdateKD_m0B61BDF3C69C5DF19B2DD495BB8D6A6938CBBEEB (void);
// 0x00000090 System.Void ScoreBoardManager::.ctor()
extern void ScoreBoardManager__ctor_mC5F46B42B4D3F4218B8A69C3CB751A45F1D6545C (void);
// 0x00000091 System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x00000092 System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x00000093 System.Void MouseLook::Start()
extern void MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541 (void);
// 0x00000094 System.Void MouseLook::Update()
extern void MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5 (void);
// 0x00000095 System.Void MouseLook::FixedUpdate()
extern void MouseLook_FixedUpdate_m014C4B701B8F521CF284DA7871FF1CF0A8F6D2FD (void);
// 0x00000096 System.Void MouseLook::.ctor()
extern void MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B (void);
// 0x00000097 System.Void Turn_Move::Start()
extern void Turn_Move_Start_m779466523F79606C853C60ED83A28E62969BF23D (void);
// 0x00000098 System.Void Turn_Move::Update()
extern void Turn_Move_Update_m2816D5E307B484F2CDB60B4CFF6374C37E48DE0D (void);
// 0x00000099 System.Void Turn_Move::.ctor()
extern void Turn_Move__ctor_m409C271E24FD807386FF2FA9A0948D5B7227D9D0 (void);
// 0x0000009A System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x0000009B UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x0000009C System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x0000009D System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x0000009E System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x0000009F System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x000000A0 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x000000A1 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x000000A2 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
static Il2CppMethodPointer s_methodPointers[162] = 
{
	Launcher_Start_mBD1E6306A64712A170916699B4BE6AA6BD32E37E,
	Launcher_Awake_m51131F9D53E49BF22DBF64A5B3CC40A475058D1A,
	Launcher_OnConnectedToMaster_mBE4CB3AA2DF5AF2BE55F7A31C7520372408F75A5,
	Launcher_OnJoinedLobby_m1B40410F13151F14C74CDF85280285C90253C6E7,
	Launcher_CreateRoom_m1F5EFF5B4E97EE8AAF3D683BF6CCCF9309A5F389,
	Launcher_OnJoinedRoom_m070B91E213700375EF7458F10138C155C3CEA22F,
	Launcher_OnMasterClientSwitched_mA2FB07F9E75ADDEB4CF269A85BB3C4C2F0D09F75,
	Launcher_JoinRoom_m5F8FE9667B304077DBBDF9F661FED2C806D5080D,
	Launcher_OnCreateRoomFailed_m69653B4F9C2BC16C4E8EFD994B023F58A34B72A7,
	Launcher_LeaveRoom_m17A2F2FB67A3080243D4450F1796708EC1D7FEBE,
	Launcher_OnLeftRoom_mC9D36826DBFF6B0059391F99B888925C59DB4B77,
	Launcher_OnRoomListUpdate_mB7315F356D99B384839DD7A9459BAF51AF941E8F,
	Launcher_OnPlayerEnteredRoom_mA47224DCA42162C343F78E7EFB5A79B66C22CCC2,
	Launcher_StartGame_m1929F43379C54CA09AF8A00DC0FE7E1B040C83E1,
	Launcher__ctor_mB177F9E0B472FC0208A5EF8E702BD3712C0AED96,
	Menu_Open_mAE2961F7F13C7085320D41A6AC36DE4055D00D32,
	Menu_Close_m36F6ECC1DE557D44B4588076226A1D71FF8451E5,
	Menu__ctor_m7EE9043A2E9DD126B6F69DD8AE3BC9CBDF6E2134,
	MenuManager_Awake_mEFBAF3F8CBDEF5A033B3BAD9CA897801135B6463,
	MenuManager_OpenMenu_m5D1B741F99822E35C5375C6DB01389A6ECB08D09,
	MenuManager_OpenMenu_mC076A53AAF0FB8F2CF531BD124FE44738C4D359A,
	MenuManager_CloseMenu_m5AAA343AF3BE130D987F7DD37E42AC8855C5D196,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	PlayerListItem_Setup_m37B55A036CBE96EEC37C300BACBA39BFC3DCC978,
	PlayerListItem_OnPlayerLeftRoom_m7BE0EAB716C6EF673C63F63B23FDCBA2310EE706,
	PlayerListItem__ctor_m7C8496D1D1F19D6A371A782E74B436B15F4A87A3,
	PlayerName_Start_mEAB6D4DA8FC9F22B98BC2BD26FB8EBD83FCEDD3F,
	PlayerName_onUserNameInputValueChanged_m250E5B03B675214EA929F3AD4FF72DAFEB14C0E9,
	PlayerName__ctor_m823B662B9836F22D25C3E8543975C64451CB2E1C,
	RoomListItem_Setup_m0D2DFC4693D420BD2A6C71703C483A1432AAD7FA,
	RoomListItem_OnClick_mFDBF25695DFA8123556FB6852F4C44B404A4F9B0,
	RoomListItem__ctor_mD67FF455921E65330604613BE05C59A33DC2E5DC,
	RoomManager_Awake_m1B0A42856399F2B323B12A1EC2313430753F2676,
	RoomManager_OnEnable_m9296F4D0D35A91B5DE105E9ACCF3A3710798AA95,
	RoomManager_OnDisable_m1AD6576F43FBB141C67E93D81EC9F3BB22D241ED,
	RoomManager_OnSceneLoaded_m913468D79B41E00B21BB4EAC7E8414077F110A99,
	RoomManager__ctor_mB80958CB1C8945EFECA6721A48CCD7FD4FD642C0,
	SpawnManager_Awake_mAA4782E6BB38FD064066134F836BFC0D1E22C5ED,
	SpawnManager_GetSpawnPoint_m05A33D472D07BB860BBFA3C47F405A583C4C809B,
	SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59,
	SpawnPoint_Awake_mF1B511096D33AF531699E45CF8DCF401EFC03BB9,
	SpawnPoint__ctor_m5A0EE1AE5C5886E3EAB22116C74D10952EA35D68,
	RotateOrb_Update_m586CE3DDA5497ABAFD489BBF8F9AEFC97C0DCF51,
	RotateOrb__ctor_m2BF45F05C309A0E575A924386251900B7BA155DE,
	Shake_Start_m1600C826C2AEC8845D2DA2C79529EB5ADF34361F,
	Shake_Update_mD4180E20FAC0B5C31E2E8F4625FA0F102EB944E6,
	Shake__ctor_mF717CF705450396F4E4D67B34334C90632133B31,
	BillBoard_Update_mD3CE536C88D1A216DFA678AF603375ADE6A7F103,
	BillBoard__ctor_mCA6D5C7FA62AD3E598CF7B07D30E81F954C9DAF8,
	CameraFollow_Update_m9762CC5B7A2D28B7B8116BA8DE2342AFB654AB08,
	CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE,
	DeleteFlash_Start_m1DEADFEE487364DBA863B51153F19C5C17025143,
	DeleteFlash_Delete_m8C1A4F0ABF27BF6C2BD480B0F6995985CC795890,
	DeleteFlash__ctor_m8F958FE65ACD1D1E644FCA416E24078212516E8F,
	U3CDeleteU3Ed__1__ctor_m216875B50A3B3025AE303EFE9ED9B99F8EA2D38F,
	U3CDeleteU3Ed__1_System_IDisposable_Dispose_mD6EBB2645B251514EB99201C802CF5BBB0A51379,
	U3CDeleteU3Ed__1_MoveNext_mC0A5143E3663F1FCE949CD9581C7F05C3BDBB72D,
	U3CDeleteU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m52760247A64E356F41E4467EABEE4D3D269FDD43,
	U3CDeleteU3Ed__1_System_Collections_IEnumerator_Reset_m42D86D8CA78EB232F7C0C5B1E3BA0FB8D7152A1B,
	U3CDeleteU3Ed__1_System_Collections_IEnumerator_get_Current_mEFA12E89EDC56CD75DFAD58CB66583DEA7B17402,
	GunControl_Start_mE1294B9657EA97AF63F6E6428D44E928BE707CF9,
	GunControl_Fire_m3D7F067AC3E8CF23ACD03B70E08F858353CF7570,
	GunControl_FireRate_mF82F43A4A5DD8F17764A4EEA1ED41934247D53C8,
	GunControl_SpreadReset_mBCF8794FEB70233376243A6DD993D62B9E0E8A95,
	GunControl_Reload_m3FA7F34FDF457C583383B420297FF8B155C45536,
	GunControl_RPC_Shoot_m1ECE74CFED1B6F35B33519958BDD58259B6E10C7,
	GunControl__ctor_m6F7C4242AC4BC4F17EB0D81974E4ACBE2FCB4001,
	U3CFireRateU3Ed__20__ctor_mF7B23521EC7A14A2C73B6C4D31233ECB83726B55,
	U3CFireRateU3Ed__20_System_IDisposable_Dispose_mAABF726483CB5951DDB3DFC7AAF7256655E80008,
	U3CFireRateU3Ed__20_MoveNext_m44CB144ABEE69651A6F58E1CAA5AEE05A279C748,
	U3CFireRateU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1323AD636CA63A9B03737772BF8485FEB063EC14,
	U3CFireRateU3Ed__20_System_Collections_IEnumerator_Reset_m8CB91B9CB3E5A883A9E900C450E4E1F69F2049CF,
	U3CFireRateU3Ed__20_System_Collections_IEnumerator_get_Current_m986D678AA608DE4C57F8194957D48CE1F57B6F97,
	U3CSpreadResetU3Ed__21__ctor_m9EA8131BA00AADDFD24366141DDC1C43DAC1D9D0,
	U3CSpreadResetU3Ed__21_System_IDisposable_Dispose_m4FC73AB61941AEC1032DBC20421080D7D3E40B67,
	U3CSpreadResetU3Ed__21_MoveNext_m5FD7B1A203B19C0E95FFEE969C83597AB307B644,
	U3CSpreadResetU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m007B89432E455B7F770CFD41E7E9106CE4250766,
	U3CSpreadResetU3Ed__21_System_Collections_IEnumerator_Reset_m16F4AA565706420FDCC40C6F1A04C19A2198190B,
	U3CSpreadResetU3Ed__21_System_Collections_IEnumerator_get_Current_mA6EA8E0EFE6130276101F09788543A37BF878654,
	U3CReloadU3Ed__22__ctor_m2201D70CCAE341BDDBEA33527A97C1800991EC7A,
	U3CReloadU3Ed__22_System_IDisposable_Dispose_m3D2BB7FB141F871A9E08A6029D6CA0B9F14B9A47,
	U3CReloadU3Ed__22_MoveNext_m5908F49995FB131887058CEF4ECA9A7A3839E67F,
	U3CReloadU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC49E3DBB145B502ACBFAC33B91DD737D815024A,
	U3CReloadU3Ed__22_System_Collections_IEnumerator_Reset_m2D383BC1403BD8A5C485E0CB6F88636A9DF28C06,
	U3CReloadU3Ed__22_System_Collections_IEnumerator_get_Current_m0E35258881CF80F7B7A70077DA7E848A471CFE50,
	NULL,
	SetLayer_Awake_m1FE4DB1B05F4E2371460F04DC5155077FFBCFFC1,
	SetLayer__ctor_m4DF136ADF2483F9A2BF84B67D0E07C0648E7B9F0,
	OrbSpawn_Start_m37455446469CD476F0FFC80A23B66BB38EDF289A,
	OrbSpawn_Update_mB09656515477DD69631E91EE4C0890C9333475AF,
	OrbSpawn_FireOrb_mEE78F143BFE55D4FD1C3B49DE21FFED1A21BF8D6,
	OrbSpawn_FixedUpdate_m6083F8D53C0D1ED1401CAA20E486C3AE44CB9119,
	OrbSpawn__ctor_m382735D6036D759826F84534769768DDCB679474,
	PlayerLook_Start_m6F42762E7F9161934F3A942F2247F73A2FFE61A5,
	PlayerLook_Update_mA425AA6701F39303BF7744B1A2E2E2B41BAC6793,
	PlayerLook_Inputs_mABDD75FEDB60BD077BFCC696EF6079933BB3B723,
	PlayerLook__ctor_mF86A13B977B997D8F4BE67197972675B6AC09456,
	PlayerManager_Awake_m4E0FE19C5F34114E28F28F04CCF33C2E34C743E7,
	PlayerManager_OnPlayerEnteredRoom_mF47D9E64F878E27349BFFB066949DFD90B4945FF,
	PlayerManager_Start_mA587FD881326FC1FBCEE2699E54A8A0ACCF83455,
	PlayerManager_CreateController_m69B81D159BF3BF6508A16733FC7725E731C47C29,
	PlayerManager_Die_m1FDAE7B1320B80ED49DFF09BFE7ABEAD58EF96F2,
	PlayerManager_UpdateKills_mDCF2BF1B361446BCFB9F1B870B7B21CB4A001FBB,
	PlayerManager_UpdateScoreBoard_mAA33450EB7DF52AB83DD993F1F54F63102E1F424,
	PlayerManager_RPC_UpdateScoreBoard_m5CF310A54C464E6042B1C29AA5AD21965C5F2B45,
	PlayerManager__ctor_m4C7CA12A8243D6CA73C1EA65B361E7B717070471,
	PlayerMovement_OnSlope_m0FEB636046795FABC82F61549935B2EE10C63568,
	PlayerMovement_Awake_m441E25AABE54B8C5068808DB8025B67B9A7EA87E,
	PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement_CanvasUpdate_m7DA2BEFBFBB51D0923215C0BB7DFF53A889D248C,
	PlayerMovement_ControlSpeed_m0B7CD52D0A6447607E08A1009138B2FB6D31C39A,
	PlayerMovement_Jump_m18B01D49691550104FB03CBFE5060DA675CC8F7E,
	PlayerMovement_Inputs_mDBF6B23AEB0F9445A81DB4CF8DD2F56C682D07DA,
	PlayerMovement_ControlDrag_m24019BCE1DC6B18B40E7D5AE26C716283AAEA619,
	PlayerMovement_FixedUpdate_m774280268A537B6ED9D9171CEAE67E9A0C3A9499,
	PlayerMovement_MovePlayer_m48A4FBFA7E90EFA604296D4294D677675C0C7786,
	PlayerMovement_EquipGun_m034ACE4BD2A95CF136F5797356BBFB14CD7F1995,
	PlayerMovement_OnPlayerPropertiesUpdate_mB3E510B1412252F562BA60EBF20F78EC4C805B2F,
	PlayerMovement_TakeDamage_m102C3EBE5A69234CD0FB69C7D54131CAA2A06BF8,
	PlayerMovement_RPC_TakeDamage_mF4BBA6C6FD6C77F01FC6A38015A213D46DDFB393,
	PlayerMovement_RPC_IncreaseKills_m806326A9DBC3539E5D25EF8EABBE515B7A61C48F,
	PlayerMovement_Die_mEE19591E6BB295276E46178F81CC568F23819B0B,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	UsernameDisplay_Start_mB27B58282D6B9D9347220C104CB0B6892E6777DB,
	UsernameDisplay__ctor_mC84E68114906E4D9E28E4D91D59320B44D929BC7,
	Wallrunning_canWallRun_mC02EC2E2722C5B9984243CB291A28113EA97BD5E,
	Wallrunning_CheckWall_m63072765AE2DE567B910A2942AB635B66A098A5B,
	Wallrunning_Start_mEF2BD7D5CFC701E75B202138CBE4E5ADD0735B95,
	Wallrunning_Update_mA733766805FA0F470575A07A07F9904FF36D541E,
	Wallrunning_StartWallRun_m3EADD4A014ABA65B010FAA0CD1658BCF86612D5C,
	Wallrunning_StopWallRun_mC2C3E2EFA26961C7379EFA9DDE4D044809A5F8ED,
	Wallrunning__ctor_mB36D52AEE4F02EC669E2E878CE0B40B1987B2F2D,
	CanvasManager_SetCanvas_m9B3B1BDF91453AD6BD36523DD58D81DCE474F30E,
	CanvasManager__ctor_mD320401BACAF61E10355C3BEDAF096DCDA817AC9,
	ScoreBoardItem_Initialize_mD0E6DF486EA62EA84BB266A22383D25297DC9F32,
	ScoreBoardItem__ctor_mD7C526C8F583A00D65B1C1E522366EE98A5C637A,
	ScoreBoardManager_Start_mEDC674AB7B64371CF0F9E8A09A05FDC864B8F24A,
	ScoreBoardManager_AddScoreBoardItem_m4FEF102126D5404B9D7503B6050E78A170B760F2,
	ScoreBoardManager_OnPlayerEnteredRoom_mB2D3B837CF2F5FA602EB9DBDA08228DF97A1B87B,
	ScoreBoardManager_OnPlayerLeftRoom_mC705709E1DF4F4BBE568282D4DA94B94863A7E5C,
	ScoreBoardManager_RemoveScoreBoardItem_m16B65D04B76E1623C067364F5EE7CA1CCC726ADC,
	ScoreBoardManager_UpdateKD_m0B61BDF3C69C5DF19B2DD495BB8D6A6938CBBEEB,
	ScoreBoardManager__ctor_mC5F46B42B4D3F4218B8A69C3CB751A45F1D6545C,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	MouseLook_Start_m699B23D66C4F21B566C48A524BC40A828F5E3541,
	MouseLook_Update_m3D49361C94E0433BB35499708EE783B4543D83D5,
	MouseLook_FixedUpdate_m014C4B701B8F521CF284DA7871FF1CF0A8F6D2FD,
	MouseLook__ctor_mD12D8075DDEA2085341B59FF8BA9FB353613200B,
	Turn_Move_Start_m779466523F79606C853C60ED83A28E62969BF23D,
	Turn_Move_Update_m2816D5E307B484F2CDB60B4CFF6374C37E48DE0D,
	Turn_Move__ctor_m409C271E24FD807386FF2FA9A0948D5B7227D9D0,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
};
static const int32_t s_InvokerIndices[162] = 
{
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	2692,
	2692,
	1371,
	3262,
	3262,
	2692,
	2692,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	2692,
	2692,
	2692,
	3262,
	2692,
	2692,
	3262,
	3262,
	3262,
	3262,
	2692,
	3262,
	3262,
	3262,
	3262,
	3262,
	1630,
	3262,
	3262,
	3195,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3195,
	3262,
	2677,
	3262,
	3225,
	3195,
	3262,
	3195,
	3262,
	3262,
	3195,
	3195,
	3195,
	2757,
	3262,
	2677,
	3262,
	3225,
	3195,
	3262,
	3195,
	2677,
	3262,
	3225,
	3195,
	3262,
	3195,
	2677,
	3262,
	3225,
	3195,
	3262,
	3195,
	1471,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	2692,
	3262,
	3262,
	3262,
	3262,
	3262,
	1026,
	3262,
	3225,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	2677,
	1602,
	1471,
	1471,
	2677,
	3262,
	3262,
	3262,
	3262,
	3225,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	650,
	3262,
	2692,
	3262,
	3262,
	2692,
	2692,
	2692,
	2692,
	1026,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3262,
	3257,
	3262,
	3262,
	2692,
	2757,
	1059,
	2692,
	3262,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	162,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
